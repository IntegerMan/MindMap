﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MindMapClasses.Models
{
    public class Topic : ICloneable
    {

        [Display(Name = "Topic Id")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Topic Name")]
        public string Name { get; set; }

        public string Description { get; set; }

        public int TopicTypeId { get; set; } = 1;

        [ForeignKey("TopicTypeId")]
        public TopicType TopicType { get; set; }

        public Topic DeepClone()
        {
            var topic = new Topic()
            {
                Description = this.Description,
                Id = this.Id,
                Name = this.Name,
                TopicTypeId = this.TopicTypeId,
                TopicType = this.TopicType
            };

            return topic;
        }

        public object Clone()
        {
            return this.DeepClone();
        }
    }
}
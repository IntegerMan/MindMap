﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MindMapClasses.Models
{
    public class TopicType : ICloneable
    {

        [Display(Name = "Topic Type Id")]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        [Display(Name = "Topic Type Name")]
        public string Name { get; set; }

        public List<Topic> Topics { get; set; }

        public TopicType DeepClone()
        {
            var topicType = new TopicType() { Id = this.Id, Name = this.Name};

            return topicType;
        }

        public object Clone()
        {
            return this.DeepClone();
        }
    }
}
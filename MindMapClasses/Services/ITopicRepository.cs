﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MindMapClasses.Models;

namespace MindMapClasses.Services
{
    public interface ITopicRepository
    {
        Task<bool> CreateTopic(Topic topic);
        Task<bool> DeleteTopic(int id);
        Task<Topic> GetTopic(int id);
        Task<IEnumerable<Topic>> GetTopics();
        Task<Topic> UpdateTopic(Topic topic);
    }
}
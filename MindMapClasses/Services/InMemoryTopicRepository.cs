﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using MindMapClasses.Models;

namespace MindMapClasses.Services
{
    public class InMemoryTopicRepository : ITopicRepository
    {
        private int _nextId = 1;
        private readonly IList<Topic> _topics = new List<Topic>();

        public async Task<bool> CreateTopic(Topic topic)
        {

            topic.Id = this._nextId++;

            this._topics.Add(topic);

            return true;
        }

        public async Task<bool> DeleteTopic(int id)
        {
            var match = this._topics.FirstOrDefault(t => t.Id == id);

            return this._topics.Remove(match);
        }

        public async Task<Topic> GetTopic(int id)
        {
            return this._topics.FirstOrDefault(t => t.Id == id);
        }

        public async Task<IEnumerable<Topic>> GetTopics()
        {
            return this._topics.ToImmutableList();
        }

        public async Task<Topic> UpdateTopic(Topic topic)
        {
            await this.DeleteTopic(topic.Id);

            this._topics.Add(topic);

            return topic;
        }
    }
}

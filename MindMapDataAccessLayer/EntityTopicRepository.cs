﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using MindMapClasses.Models;
using MindMapClasses.Services;

namespace MindMapDataAccessLayer 
{
    public class EntityTopicRepository : ITopicRepository
    {
        private readonly TopicsContext _context;

        public EntityTopicRepository(TopicsContext context)
        {
            this._context = context;
        }

        public async Task<bool> CreateTopic(Topic topic)
        {
            var entry = this._context.Topics.Add(topic);

            this._context.SaveChanges();

            return entry != null;
        }

        public async Task<bool> DeleteTopic(int id)
        {
            var match = this._context.Topics.FirstOrDefault(t => t.Id == id);

            if (match is null)
            {
                return false;
            }

            this._context.Topics.Remove(match);

            this._context.SaveChanges();

            return true;
        }

        public async Task<Topic> GetTopic(int id)
        {
            return this._context.Topics.FirstOrDefault(t => t.Id == id);
        }

        public async Task<IEnumerable<Topic>> GetTopics()
        {
            return this._context.Topics.ToImmutableList();
        }

        public async Task<Topic> UpdateTopic(Topic topic)
        {
            var match = this._context.Topics.FirstOrDefault(t => t.Id == topic.Id);

            if (match is null)
            {
                return null;
            }

            this._context.Topics.Update(match);

            match.Name = topic.Name;
            match.Description = topic.Description;

            this._context.SaveChanges();

            return match;
        }
    }
}

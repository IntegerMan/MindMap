﻿using Microsoft.EntityFrameworkCore;
using MindMapClasses.Models;

namespace MindMapDataAccessLayer
{
    public class TopicsContext : DbContext
    {
        public TopicsContext(DbContextOptions<TopicsContext> options) : base(options)
        {
        }

        public DbSet<Topic> Topics { get; set; }
        public DbSet<TopicType> TopicTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Topic>().HasOne(t => t.TopicType).WithMany(tt => tt.Topics);
            modelBuilder.Entity<Topic>().Property(t => t.Name).IsRequired();

            base.OnModelCreating(modelBuilder);
        }
    }
}
﻿
If not exists (select 1 from dbo.TopicTypes) begin

	Set Identity_Insert [dbo].[TopicTypes] On

	Insert Into [dbo].[TopicTypes] (Id, Name) Values (1, 'Idea'), (2, 'Work Item'), (3, 'Question')

	Set Identity_Insert [dbo].[TopicTypes] Off

End 
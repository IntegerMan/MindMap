﻿ALTER TABLE [dbo].[Topics]
	ADD CONSTRAINT [Topics_TopicTypes]
	FOREIGN KEY ([TopicTypeID])
	REFERENCES dbo.TopicTypes (Id)

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MindMapClasses.Models;
using MindMapClasses.Services;

namespace MindMapWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Topics")]
    public class TopicsApiController : Controller
    {
        private readonly ITopicRepository _topicRepository;

        public TopicsApiController(ITopicRepository topicRepository)
        {
            _topicRepository = topicRepository;
        }

        // GET: api/Topics
        [HttpGet]
        public async Task<IEnumerable<Topic>> Get()
        {
            return await this._topicRepository.GetTopics();
        }

        // GET: api/Topics/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<Topic> Get(int id)
        {
            return await this._topicRepository.GetTopic(id);
        }
        
        // POST: api/Topics
        [HttpPost]
        public async Task<bool> Post([FromBody]Topic topic)
        {
            return await this._topicRepository.CreateTopic(topic);
        }
        
        // PUT: api/Topics/5
        [HttpPut("{id}")]
        public async void Put(int id, [FromBody]Topic value)
        {
            await this._topicRepository.UpdateTopic(value);
        }
        
        // DELETE: api/Topics/5
        [HttpDelete("{id}")]
        public async void Delete(int id)
        {
            await this._topicRepository.DeleteTopic(id);
        }
    }
}

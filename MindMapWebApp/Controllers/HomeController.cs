﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MindMapWebApp.Models;

namespace MindMapWebApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "This is a learning application for refreshing / gearing up on ASP .NET Core, MVC, Web API, etc.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Please don't contact me about this app; it's not worth either of our time.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

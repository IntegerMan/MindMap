﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MindMapClasses.Models;
using MindMapClasses.Services;
using MindMapWebApp.Services;

namespace MindMapWebApp.Controllers
{
    public class TopicsController : Controller
    {
        private readonly ITopicRepository _topicRepository;

        public TopicsController(IConfiguration configuration, ITopicRepository topicRepository)
        {
            _topicRepository = topicRepository;

        }

        // GET: Topics
        public async Task<ActionResult> Index()
        {
            IEnumerable<Topic> model = new List<Topic>();
            model = await _topicRepository.GetTopics();

            return View(model);
        }

        // GET: Topics/Details/5
        public async Task<ActionResult> Details(int id)
        {
            Topic topic = await GetTopic(id);

            if (topic == null)
            {
                return new NotFoundResult();
            }

            return View(topic);
        }

        // GET: Topics/Create
        public ActionResult Create()
        {
            // This shouldn't need to talk to the Web API as we're just displaying an in-place default page
            var topic = new Topic();

            return View(topic);
        }

        // POST: Topics/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(IFormCollection collection)
        {

            try
            {
                var topic = new Topic();

                // Copy form values into the new item
                UpdateTopicFromForm(topic, collection);

                await _topicRepository.CreateTopic(topic);

                return RedirectToAction(nameof(Index));

            }
            catch (Exception ex)
            {
                throw new Exception("The topic could not be created", ex);
            }
        }

        // GET: Topics/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            Topic topic = await GetTopic(id);

            if (topic == null)
            {
                return new NotFoundResult();
            }

            return View(topic);
        }

        private async Task<Topic> GetTopic(int id)
        {
            var topic = await _topicRepository.GetTopic(id);

            return topic;
        }

        // POST: Topics/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, IFormCollection collection)
        {
            Topic topic = await GetTopic(id);
            if (topic == null)
            {
                return new NotFoundResult();
            }

            try
            {

                // Copy form values into the item
                UpdateTopicFromForm(topic, collection);

                await _topicRepository.UpdateTopic(topic);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View(topic);
            }
        }

        // GET: Topics/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            var topic = await GetTopic(id);
            if (topic == null)
            {
                return new NotFoundResult();
            }

            return View(topic);
        }

        // POST: Topics/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, IFormCollection collection)
        {
            if (await _topicRepository.DeleteTopic(id))
            {
                return RedirectToAction(nameof(Index));
            }

            // Let's just show the delete dialog again? Not sure the ideal way to handle this failure.
            return View(await GetTopic(id));
        }
        
        private static void UpdateTopicFromForm(Topic topic, IFormCollection collection)
        {
            topic.Name = collection[nameof(topic.Name)];
            topic.Description = collection[nameof(topic.Description)];
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MindMapClasses.Models;
using MindMapClasses.Services;
using Newtonsoft.Json;

namespace MindMapWebApp.Services 
{
    public class ApiTopicRepository : ITopicRepository
    {
        private readonly Uri _apiUri;

        public ApiTopicRepository()
        {
            // TODO: This should go in a config file somewhere
            this._apiUri = new Uri("http://localhost:52568");
        }

        public async Task<bool> CreateTopic(Topic topic)
        {
            using (var client = CreateHttpClient())
            {

                // Post the topic
                var response = await client.PostAsync("api/topics", BuildJsonContent(topic));

                if (response.IsSuccessStatusCode)
                {
                    return GetDataFromResponse<bool>(response);
                }

                return false;
            }
        }

        private static StringContent BuildJsonContent(Topic topic)
        {
            return new StringContent(JsonConvert.SerializeObject(topic), Encoding.UTF8, "application/json");
        }

        public async Task<bool> DeleteTopic(int id)
        {
            using (var client = CreateHttpClient())
            {

                // Post the topic
                var response = await client.DeleteAsync($"api/topics/{id}");

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }

            }

            return false;
        }

        public async Task<Topic> GetTopic(int id)
        {
            using (var client = CreateHttpClient())
            {
                // Grab the topic
                var response = await client.GetAsync($"api/topics/{id}");

                if (response.IsSuccessStatusCode)
                {
                    return GetDataFromResponse<Topic>(response);
                }

            }

            return null;
        }

        public async Task<IEnumerable<Topic>> GetTopics()
        {
            using (var client = CreateHttpClient())
            {

                // Grab the list of topics
                var response = await client.GetAsync("api/topics");

                if (response.IsSuccessStatusCode)
                {
                    return GetDataFromResponse<IEnumerable<Topic>>(response);
                }

            }

            return new List<Topic>();
        }

        public async Task<Topic> UpdateTopic(Topic topic)
        {
            using (var client = CreateHttpClient())
            {

                // Put the topic
                var response = await client.PutAsync($"api/topics/{topic.Id}", BuildJsonContent(topic));

                if (response.IsSuccessStatusCode)
                {
                    return topic;
                }

                return null;
            }
        }

        private HttpClient CreateHttpClient()
        {
            var client = new HttpClient {BaseAddress = _apiUri};
            
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

        private static T GetDataFromResponse<T>(HttpResponseMessage response)
        {
            var data = response.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<T>(data);
        }
    }
}
